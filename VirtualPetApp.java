import java.util.Scanner;

public class VirtualPetApp {
	public static void main(String[] args){
		
		Bird[] flock = new Bird[1];
		
		for(int i = 0; i < flock.length; i++){
			Scanner reader = new Scanner(System.in);
			System.out.println("give a name to the bird");
			String name = reader.nextLine();
		
			System.out.println("does the bird eat seeds or worms?");
			String food = reader.nextLine();
			
			System.out.println("what color is the bird?");
			String colour = reader.nextLine();
			
			flock[i] = new Bird(name, food, colour);

			reader.close();
		}
		
		System.out.println(flock[flock.length - 1].getName());
		System.out.println(flock[flock.length - 1].getFood());
		System.out.println(flock[flock.length - 1].getColour());

		flock[flock.length - 1].setColour("Gold");

		System.out.println(flock[flock.length - 1].getName());
		System.out.println(flock[flock.length - 1].getFood());
		System.out.println(flock[flock.length - 1].getColour());
	}
}
