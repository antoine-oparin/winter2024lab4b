public class Bird{

    private String name;
    private String food;
    private String colour; 
	
	public Bird(String name, String food, String colour){
        this.name = name;
        this.food = food;
        this.colour = colour;
    }
	public String isVegeterian(){
	    if(this.food.equals("seeds"))
		    return "I am vegetarian!";
	    else
		    return "I am not vegetarian";
	}
	public String colourOfFeathers(){
		return "My feathers are " + this.colour + " !"; 
	}

	public void setColour(String colour) {
		this.colour = colour;
	}

	public String getName() {
		return name;
	}

	public String getFood() {
		return food;
	}

	public String getColour() {
		return colour;
	}
}
